import java.util.Scanner;

public class MenuContas<opcao> {
    Admin admin = new Admin();
    Banco banco = new Banco();

    public void AbrirConta(Admin admin) {
        Scanner proprietario = new Scanner(System.in);
        System.out.print("Nome do proprietario: ");
        String nome = proprietario.nextLine();
        admin.AbrirConta(nome);

    int opcao = 1;

        do {
            System.out.println("********************************");
            System.out.println("   Abertura de conta no Banco   ");
            System.out.println(" 1.  Conta Poupança             ");
            System.out.println(" 2.  Conta Corrente             ");
            System.out.println(" 3.  Conta Salário              ");
            System.out.println(" 0.  Voltar ao Menu Principal   ");
            System.out.println("********************************");

            Scanner menu = new Scanner(System.in);
            System.out.print("Digite a operação desejada: ");
            opcao = menu.nextInt();

            switch (opcao) {
                case 1:
                    admin.AbrirContaPoupanca(nome);
                    break;
                case 2:
                    admin.AbrirContaCorrente(nome);
                    break;
                case 3:
                    admin.AbrirContaSalario(nome);
                    break;
                case 0:
                    break;
            }
        } while (opcao != 0);
    }
    public void Sacar(Admin admin) {
        Scanner proprietario = new Scanner(System.in);
        System.out.print("Nome do executante: ");
        String nomeProprietario = proprietario.nextLine();

        int opcao = 1;

        do {
            System.out.println("********************************");
            System.out.println("   Abertura de conta no Banco   ");
            System.out.println(" 1.  Conta Poupança             ");
            System.out.println(" 2.  Conta Corrente             ");
            System.out.println(" 3.  Conta Salário              ");
            System.out.println(" 0.  Voltar ao Menu Principal   ");
            System.out.println("********************************");

            Scanner menu = new Scanner(System.in);
            System.out.print("Digite a operação desejada: ");
            opcao = menu.nextInt();

            switch (opcao) {
                case 1:
                    admin.SacarPoupanca(banco.nomeProprietario);
                    break;
                case 2:
                    admin.SacarCorrente(banco.nomeProprietario);
                    break;
                case 3:
                    admin.SacarSalario(banco.nomeProprietario);
                    break;
                case 0:
                    break;
            }
        } while (opcao != 0);

    }
    public void Depositar(Admin admin) {
        Scanner proprietario = new Scanner(System.in);
        System.out.print("Nome do executante: ");
        String nomeProprietario = proprietario.nextLine();
    }

    public void print(Admin admin) {
        int tamanho = admin.contas.size();
        for (int i = 0; i < tamanho; i++) {
            System.out.println(admin.contas.get(i).getNomeProprietario());

            int opcao = 1;

            do {
                System.out.println("********************************");
                System.out.println("   Abertura de conta no Banco   ");
                System.out.println(" 1.  Conta Poupança             ");
                System.out.println(" 2.  Conta Corrente             ");
                System.out.println(" 3.  Conta Salário              ");
                System.out.println(" 0.  Voltar ao Menu Principal   ");
                System.out.println("********************************");

                Scanner menu = new Scanner(System.in);
                System.out.print("Digite a operação desejada: ");
                opcao = menu.nextInt();

                switch (opcao) {
                    case 1:
                        admin.DepositarPoupanca(banco.nomeProprietario);
                        break;
                    case 2:
                        admin.DepositarCorrente(banco.nomeProprietario);
                        break;
                    case 3:
                        admin.DepositarSalario(banco.nomeProprietario);
                        break;
                    case 0:
                        break;
                }
            } while (opcao != 0);
        }
    }

    public void transferir(Admin admin) {
        Scanner proprietario = new Scanner(System.in);
        System.out.print("Nome do executante: ");
        String nomeProprietario = proprietario.nextLine();

        System.out.print("Digite a conta destino : ");
        String destino = proprietario.nextLine();

        int opcao = 0;
        do {
            System.out.println("*******************************");
            System.out.println("       Tipo da transação       ");
            System.out.println("  1.  Conta Poupança           ");
            System.out.println("  2.  Conta Corrente           ");
            System.out.println("  3.  Conta Salário            ");
            System.out.println("  4.  Voltar ao Menu Principal ");
            System.out.println("*******************************");

            Scanner menu = new Scanner(System.in);
            System.out.print("Tipo da conta origem: ");
            Integer tipoConta = menu.nextInt();

            System.out.print("Tipo da conta destino: ");
            Integer tipoContaDestino = menu.nextInt();

            System.out.print("Digite o valor a ser tranferido: ");
            Double valor = menu.nextDouble();

            admin.transferir(tipoConta, tipoContaDestino, valor, nomeProprietario, destino);
            break;
        } while (opcao < 2);
    }
}