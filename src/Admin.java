import java.util.ArrayList;
import java.util.Scanner;

public class Admin {

    int numeroConta = 100000;

    protected ArrayList<Banco> contas = new ArrayList<>();

    public void AbrirConta(String nome) {
        Banco banco = new Banco();
        banco.setNomeProprietario(nome);
        contas.add(banco);
        System.out.print(contas.size());
    }

    public void AbrirContaPoupanca(String nome){
        for (Banco conta : this.contas){
            if (conta.getNomeProprietario().equals(nome)){
                Scanner proprietario = new Scanner(System.in);
                System.out.println("Saldo inicial: ");
                double saldo = proprietario.nextDouble();

                System.out.println("Data de aniversário do proprietário: ");
                int dia = proprietario.nextInt();

                conta.AbrirContaPoupanca(this.numeroConta, 12, "MentBank", saldo, dia, 0.05);
            }
        }
    }

    public void AbrirContaCorrente(String nome){
        for (Banco conta : this.contas){
            if (conta.getNomeProprietario().equals(nome)){
                Scanner proprietario = new Scanner(System.in);
                System.out.println("Saldo inicial: ");
                double saldo = proprietario.nextDouble();

                System.out.println("Qual o valor do cheque especial para a conta? ");
                double chequeEspecial = proprietario.nextDouble();

                conta.AbrirContaCorrente(this.numeroConta,20,"MentBank",saldo,chequeEspecial);
                this.numeroConta++;
            }
        }
    }

    public void AbrirContaSalario(String nome){
        for (Banco conta : this.contas) {
            if (conta.getNomeProprietario().equals(nome)) {
                Scanner proprietario = new Scanner(System.in);
                System.out.print("Qual o saldo da Conta? ");
                Double saldo = proprietario.nextDouble();

                System.out.println("Qual a quantidades de saques mensais para a conta?");
                int numLimiteSaques = proprietario.nextInt();

                conta.AbrirContaSalario(this.numeroConta, 40, "MentBank", saldo, numLimiteSaques);
                this.numeroConta++;
            }
        }
    }

    public void ContasCadastradas() {
        System.out.println(" Clientes já cadastrados ");
        for (Banco conta : this.contas) {
            System.out.println(
                    "Nome: " +
                            conta.getNomeProprietario() +
                            " Saldo da conta: " +
                            conta.Saldo()
            );
        }
    }

    public void SacarPoupanca(String nome) {
        for (Banco conta : this.contas) {
            if (conta.getNomeProprietario().equals(nome)) {
                System.out.println(conta.getNomeProprietario() + " - " + conta.contaPoupanca.getSaldo());

                Scanner proprietario = new Scanner(System.in);

                System.out.print("Digite o valor do saque: ");
                Double saque = proprietario.nextDouble();
                conta.sacarContaPoupanca(saque);
            }
        }
    }

    public void DepositarPoupanca(String nome) {
        for (Banco conta : this.contas) {
            if (conta.getNomeProprietario().equals(nome)) {

                Scanner proprietario = new Scanner(System.in);

                System.out.print("Digite o valor de deposito: ");
                Double deposito = proprietario.nextDouble();
                conta.DepositarContaPoupanca(deposito);
            }
        }
    }

    public void SacarSalario(String nome) {
        for (Banco conta : this.contas) {
            if (conta.getNomeProprietario().equals(nome)) {

                Scanner proprietario = new Scanner(System.in);

                System.out.print("Digite o valor do saque: ");
                Double saque = proprietario.nextDouble();
                conta.SacarContaSalario(saque);
            }
        }
    }

    public void DepositarSalario(String nome) {
        for (Banco conta : this.contas) {
            if (conta.getNomeProprietario().equals(nome)) {

                Scanner proprietario = new Scanner(System.in);

                System.out.print("Digite o valor de deposito: ");
                Double deposito = proprietario.nextDouble();
                conta.DepositarContaSalario(deposito);
            }
        }
    }

    public void SacarCorrente(String nome) {
        for (Banco conta : this.contas) {
            if (conta.getNomeProprietario().equals(nome)) {

                Scanner proprietario = new Scanner(System.in);

                System.out.print("Digite o valor do saque: ");
                Double saque = proprietario.nextDouble();
                conta.SacarContaCorrente(saque);
            }
        }
    }

    public void DepositarCorrente(String nome) {
        for (Banco conta : this.contas) {
            if (conta.getNomeProprietario().equals(nome)) {

                Scanner proprietario = new Scanner(System.in);

                System.out.print("Digite o valor de deposito: ");
                Double deposito = proprietario.nextDouble();
                conta.DepositarContaCorrente(deposito);
            }
        }
    }

    public void Saldo() {
        Scanner proprietario = new Scanner(System.in);
        System.out.print("Digite o nome do proprietario: ");
        String nome = proprietario.nextLine();

        for (Banco conta : this.contas) {
            if (conta.getNomeProprietario().equals(nome)) {
                System.out.println(conta.getNomeProprietario());

                System.out.println(conta.Saldo());
            }
        }
    }

    public void transferir(Integer tipoDeConta, Integer tipoDeContaDestino, Double valor, String nome, String destino) {
        for (Banco conta : contas) {
            if (conta.getNomeProprietario().equals(nome)) {
                conta.transferir(valor, tipoDeConta, "sacar");
            }
            if (conta.getNomeProprietario().equals(destino)) {
                conta.transferir(valor, tipoDeContaDestino, "depositar");
            }
        }
    }
}
