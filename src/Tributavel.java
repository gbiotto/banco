public interface Tributavel {
    public void Sacar(double valor);
    public void Depositar(double valor);
    public double getSaldo();
}