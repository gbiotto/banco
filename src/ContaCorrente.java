public class ContaCorrente extends Conta implements Tributavel {
    protected double chequeEspecial;

    @Override
    public String toString() {
        return super.toString() +  "ContaCorrente{" +
                "chequeEspecial=" + chequeEspecial +
                '}';
    }

    public ContaCorrente(int numero, int agencia, String banco, double saldo, double chequeEspecial) {
        super(numero, agencia, banco, saldo);
        this.chequeEspecial = chequeEspecial;
    }

    public double getSaldo(){
        return (this.chequeEspecial + this.saldo);
    }

    public void Sacar(double saque) {
        if(saque <= (this.saldo+this.chequeEspecial)){
            this.saldo -= saque;
        }
        else
            System.out.println("Saldo indisponível");
    }

    public void Depositar(double deposito) {
        this.saldo = this.saldo + deposito;
    }

    public void transferir(Conta destino, Double valor) {
        if (valor <= this.getSaldo()) {
            this.saldo -= valor;
            destino.saldo += valor;
        } else
            System.out.println("Saldo indisponivel");
    }
}
