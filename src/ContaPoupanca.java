public class ContaPoupanca extends Conta{

    private int dataAniversario;
    private double taxaJuros;

    public ContaPoupanca(int numero, int agencia, String banco, double saldo, int dataAniversario, double taxaJuros) {
        super(numero, agencia, banco, saldo);
        this.dataAniversario = dataAniversario;
        this.taxaJuros = taxaJuros;
    }

    @Override
    public String toString() {
        return super.toString() + "ContaPoupanca{" +
                "dataAniversario=" + dataAniversario +
                ", taxaJuros=" + taxaJuros +
                '}';
    }

    public double getSaldo() {
        return this.saldo + this.taxaJuros*this.saldo;
    }

    public void Sacar(double saque){
        if (saque < this.saldo) {
            this.saldo -= saque;
        }
        else
            System.out.println("Saldo indisponível");
    }

    public void Depositar (double valor) {
        this.saldo+=valor;
    }

    public void transferir(Conta destino, Double valor){
        if(valor <= this.getSaldo()){
            this.saldo -= valor;
            destino.saldo += valor;
        }
        else
            System.out.println("Saldo indisponivel");
    }
}
