import java.util.*;
import java.util.stream.Stream;

public class TestaConta {
    public static void main(String[] args) {

        Cliente cli1 = new Cliente("Giovane", true, "123", 10);
        Cliente cli2 = new Cliente("Mentorama", true, "abc", 30);
        Cliente cli3 = new Cliente("Brasil", false, "cvb", 45);
        Cliente cli4 = new Cliente("Isabela", true, "ilu", 80);
        Cliente cli5 = new Cliente("Marcos", true, "dgh", 20);
        Cliente cli6 = new Cliente("Fabiano", false, "ryu", 60);
        Cliente cli7 = new Cliente("Lais", false, "chu", 74);
        Cliente cli8 = new Cliente("Roberta", true, "845", 40);
        Cliente cli9 = new Cliente("Robson", false, "rla", 60);
        Cliente cli10 = new Cliente("Carla", true, "100s", 50);
        Cliente cli11 = new Cliente("José", false, "657", 5);
        Cliente cli12 = new Cliente("Adriano", true, "aps", 36);
        Cliente cli13 = new Cliente("Thomas", false, "ttk", 20);
        Cliente cli14 = new Cliente("Bruno", false, "prg", 16);
        Cliente cli15 = new Cliente("Regiane", false, "rgfd", 32);

        List<Cliente> clientes = Arrays.asList(cli1, cli2, cli3, cli4, cli5, cli6, cli7, cli8, cli9, cli10, cli11, cli12, cli13, cli14, cli15);

        Stream<Cliente> stream = clientes.stream().filter(cliente -> cliente.getCompras() > 20);

        Optional<Cliente> maxCompras = clientes.stream().max(Comparator.comparingInt(Cliente::getCompras));
        Optional<Cliente> minCompras = clientes.stream().min(Comparator.comparingInt(Cliente::getCompras));
        OptionalDouble avgCompras = clientes.stream().mapToInt(Cliente::getCompras).average();

        System.out.println(maxCompras);
        System.out.println(minCompras);
        System.out.println(avgCompras);
    }
}
