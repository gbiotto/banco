import java.util.Scanner;

public class MenuPrincipal {

    public static void main (String[] args){

        MenuContas menuContas = new MenuContas();
        Admin admin = new Admin();

        int opcao=1;
        do{
            System.out.println("*********************************");
            System.out.println("      -- Banco Mentorama --      ");
            System.out.println(" 1.  Criar Conta                 ");
            System.out.println(" 2.  Sacar                       ");
            System.out.println(" 3.  Depositar                   ");
            System.out.println(" 4.  Mostrar Clientes            ");
            System.out.println(" 5.  Transferir valores          ");
            System.out.println(" 6.  Mostrar montante nas contas ");
            System.out.println(" 0.  Sair   :(                   ");
            System.out.println("*********************************");

            Scanner ler = new Scanner(System.in);
            System.out.print("Digite a operação desejada: ");
            opcao = ler.nextInt();

            switch(opcao){
                case 1:
                    menuContas.AbrirConta(admin);
                    break;
                case 2:
                    menuContas.Sacar(admin);
                    break;
                case 3:
                    menuContas.Depositar(admin);
                    break;
                case 4:
                    admin.ContasCadastradas();
                    break;
                case 5:
                    menuContas.transferir(admin);
                    break;
                case 6:
                    admin.Saldo();
                    break;
                case 0:
                    System.out.println("Você saiu do nosso programa :(");
                    break;
                default:
                    System.out.println("Opção inválida!");
                    break;
            }
        }while(opcao!=0);
    }
}