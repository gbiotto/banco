public class Banco {

    protected String nomeProprietario;
    protected ContaPoupanca contaPoupanca;
    protected ContaCorrente contaCorrente;
    protected ContaSalario contaSalario;
    protected Conta transferir;

    public String getNomeProprietario() {
        return nomeProprietario;
    }

    public void setNomeProprietario(String nomeProprietario) {
        this.nomeProprietario = nomeProprietario;
    }

    public void AbrirContaPoupanca(int numeroConta, int agencia, String banco, double saldo, int dataAniversario, double taxaJuros) {
        this.contaPoupanca = new ContaPoupanca(numeroConta, agencia, banco, saldo, dataAniversario, taxaJuros);
    }

    public void AbrirContaCorrente(int numeroConta, int agencia, String banco, double saldo, double chequeEspecial) {
        this.contaCorrente = new ContaCorrente(numeroConta, agencia, banco, saldo, chequeEspecial);
    }

    public void AbrirContaSalario(int numeroConta, int agencia, String banco, double saldo, int numLimiteSaques) {
        this.contaSalario = new ContaSalario(numeroConta, agencia, banco, saldo, numLimiteSaques);
    }

    public void sacarContaPoupanca(double saque) {
        this.contaPoupanca.Sacar(saque);
        System.out.println(this.contaPoupanca.getSaldo());
    }

    public void DepositarContaPoupanca(double deposito) {
        this.contaPoupanca.Depositar(deposito);
        System.out.println(this.contaPoupanca.getSaldo());
    }

    public void SacarContaCorrente(double valor) {
        this.contaCorrente.Sacar(valor);
        System.out.println(this.contaCorrente.getSaldo());
    }

    public void DepositarContaCorrente(Double deposito) {
        this.contaCorrente.Depositar(deposito);
        System.out.println(this.contaCorrente.getSaldo());
    }

    public void SacarContaSalario(double valor) {
        this.contaSalario.Sacar(valor);
        System.out.println(this.contaSalario.getSaldo());
    }

    public void DepositarContaSalario(double deposito) {
        this.contaSalario.Depositar(deposito);
        System.out.println(this.contaSalario.getSaldo());
    }

    public void transferir(Double valor, Integer tipoConta, String operacao) {
        String sacar = "sacar";
        String depositar = "depositar";


        if (sacar.equals(operacao)) {
            switch (tipoConta) {
                case 1:
                    this.contaPoupanca.Sacar(valor);
                    break;
                case 2:
                    this.contaCorrente.Sacar(valor);
                    break;
                case 3:
                    this.contaSalario.Sacar(valor);
                    break;
                default:
                    System.out.println("Opção inválida");
            }
        }

        if (depositar.equals(operacao)) {
            switch (tipoConta) {
                case 1:
                    this.contaPoupanca.Depositar(valor);
                    break;
                case 2:
                    this.contaCorrente.Depositar(valor);
                    break;
                case 3:
                    this.contaSalario.Depositar(valor);
                    break;
                default:
                    System.out.println("Opção inválida");
            }
        }
    }

    public Double Saldo() {
        Double saldo = 0.0;
        if (contaPoupanca != null) {
            saldo += contaPoupanca.getSaldo();
        }
        if (contaCorrente != null) {
            saldo += contaCorrente.getSaldo();
        }
        if (contaSalario != null) {
            saldo += contaSalario.getSaldo();
        }
        return saldo;
    }

    public int numeroConta() {
        int numero = 0;
        if (contaPoupanca != null) {
            numero += contaPoupanca.getNumero();
        }
        if (contaCorrente != null) {
            numero += contaCorrente.getNumero();
        }
        if (contaSalario != null) {
            numero += contaSalario.getNumero();
        }
        return numero;
    }
}
