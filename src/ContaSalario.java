public class ContaSalario extends Conta implements Tributavel {
    protected int numLimiteSaques;

    public ContaSalario(int numero, int agencia, String banco, double saldo, int numLimiteSaques) {
        super(numero, agencia, banco, saldo);
        this.numLimiteSaques = numLimiteSaques;
    }

    public double getSaldo(){
        return this.saldo;
    }

    public void Depositar(double deposito){
        this.saldo += deposito;
    }

    public void Sacar(double saque){
        if(saque>this.saldo){
            System.out.println("Valor indisponível");
        }
        else{
            if(this.numLimiteSaques > 0){
                this.numLimiteSaques --;
                this.saldo -= saque;
            }
            else{
                System.out.println("Limite de saque excedido");
            }
        }
    }

    public void transferir(Conta destino, Double valor) {
        if(valor < this.getSaldo()){
            this.saldo -= valor;
            destino.saldo += valor;
        }
        else
            System.out.println("Saldo indisponivel");
    }
}
